Contains many helper functions and code.

| Files                                                         | Description |
| --------------------------------------------------------------| ----------- |
| `experiment_config.py`                                        | Description of a model to be trained and validated. See the [wiki](https://gitlab.psi.ch/ML/bellotti_r/wikis/training-a-model) for details. |
| `ml_helper_functions.py`                                      | Mainly constants about the datasets. Make sure you get the correct QOI list. |
| `surrogate_performance.py`                                    | Calculates the residuals and relative errors (WITHOUT taking the absolute value). Also performs some plots of the model performance. |
| `training.py`                                                 | Contains code for training a forward surrogate model.|