import os
import numpy as np
import pandas as pd
import scipy
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
import tensorflow as tf
from sklearn.model_selection import train_test_split, KFold
from mllib.model import KerasSurrogate
from helper_functions.scan_helper_functions import AdjustedRSquared
from helper_functions.ml_helper_functions import RSquaredSeparated, AdjustedRSquaredSeparated
from helper_functions.invertible_neural_network import InvertibleNetworkSurrogate
from sklearn.metrics import mean_absolute_error, mean_squared_error, explained_variance_score,mean_absolute_percentage_error
import argparse

# +
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--test_data_file', type = str, default ='none', help = 'Path to the folder containing test data')
	parser.add_argument('--dir', type = str, default ='none', help = 'Path to the folder, where models are stored')
	seed = 49857
	tf.random.set_seed(seed)
	np.random.seed(seed)
	
	args = parser.parse_args()
	test_datadir = args.test_data_file
	test_datafile = test_datadir +'test_dataset.hdf5'
	
	model_name = 'invertible_model'
	val_model_name = 'forward_model'
	
	directory = args.dir
	test_datafile = args.test_data_file
	plot_dir = directory+"/plots/model"
	if not os.path.exists(plot_dir):
	    os.makedirs(plot_dir)
	model_dir = directory+"/model"
	custom_objects = {'AdjustedRSquared': AdjustedRSquared}
	kwargs = {'custom_objects': custom_objects,'compile': False}
	surr = InvertibleNetworkSurrogate.load(model_dir, model_name, model_kwargs=kwargs)
	dvar_test = pd.read_hdf(test_datafile,key = 'dvar')
	qoi_test = pd.read_hdf(test_datafile,key = 'qoi')
	dvar_test.reset_index(drop=True, inplace=True)
	qoi_test.reset_index(drop=True, inplace=True)
	qoi_columns = qoi_test.columns
	dvar_columns = dvar_test.columns
	qoi_pred_fw = surr.predict(dvar_test.values)
	qoi_pred_fw = pd.DataFrame(data=qoi_pred_fw, columns=qoi_columns)
	n_tries = 32
	dvar_pred_iv = surr.sample_n_tries(qoi_test.values, batch_size=128, n_tries=n_tries)
	dvar_pred_iv = pd.DataFrame(data=dvar_pred_iv, columns=dvar_columns)

	qoi_pred_fw.to_hdf(f'{model_dir}/pred.hdf5', key='fw')
	dvar_pred_iv.to_hdf(f'{model_dir}/pred.hdf5', key='inv')

'''
	# Calculate metrics:
	# R2:
	metric_fw = RSquaredSeparated()
	r2_adj_fw = metric_fw.call(qoi_test.values, qoi_pred_fw.values).numpy()
	r2_adj_fw = pd.Series(data=r2_adj_fw, index=qoi_columns)
	r2_adj_fw = np.round(r2_adj_fw, decimals=4)
	r2_adj_fw = pd.DataFrame(r2_adj_fw).T
	metric_iv = RSquaredSeparated()
	r2_adj_iv = metric_iv.call(dvar_test.values, dvar_pred_iv.values).numpy()
	r2_adj_iv = pd.Series(data=r2_adj_iv, index=dvar_columns)
	r2_adj_iv = np.round(r2_adj_iv, decimals=2)
	r2_adj_iv = pd.DataFrame(r2_adj_iv).T
	# relative and absolute error:
	abs_error_fw = np.abs( (qoi_test) - (qoi_pred_fw))
	rel_error_fw = abs_error_fw / ((qoi_test)) * 100.
	rel_error_fw = pd.DataFrame(rel_error_fw, columns=qoi_columns).abs()
	abs_error_table_fw =  np.round(abs_error_fw.quantile([0.5, 0.75, 0.9, 0.95, 0.99]).rename(index={0.5: '50%', 0.75: '75%',0.9: '90%', 0.95: '95%', 0.99: '99%',}), decimals=2)
	rel_error_table_fw = np.round(rel_error_fw.quantile([0.5, 0.75, 0.9, 0.95, 0.99]).rename(index={ 0.5: '50%',0.75: '75%',0.9: '90%', 0.95: '95%',0.99: '99%',}), decimals=2)
	P11_532_columns = []
	P12_532_columns = []
	for name in qoi_columns:
		if 'P11' in name:
			P11_532_columns.append(name)
		if 'P12' in name:
			P12_532_columns.append(name)
	abs_error_table_fw_P11 = abs_error_table_fw[P11_532_columns]
	abs_error_table_fw_P12 = abs_error_table_fw[P12_532_columns]
	rel_error_table_fw_P11 = rel_error_table_fw[P11_532_columns]
	rel_error_table_fw_P12 = rel_error_table_fw[P12_532_columns]
	abs_error_iv = np.abs(dvar_test-dvar_pred_iv)
	rel_error_iv = abs_error_iv / (dvar_test) * 100.
	rel_error_iv = pd.DataFrame(rel_error_iv, columns=dvar_columns).abs()
	abs_error_table_iv = np.round(abs_error_iv.quantile([0.5, 0.75, 0.9, 0.95, 0.99]).rename(index={0.5: '50%',0.75: '75%',0.9: '90%',0.95: '95%',0.99: '99%',}), decimals=4)
	rel_error_table_iv = np.round(rel_error_iv.quantile([0.5, 0.75, 0.9, 0.95, 0.99]).rename(index={ 0.5: '50%', 0.75: '75%', 0.9: '90%', 0.95: '95%', 0.99: '99%',}), decimals=4)
	# MEan absolute error, Mean absolute percentage error
	MAE_all_dvar = []
	MAPE_all_dvar = []
	for col in dvar_test.keys():
		MAE = mean_absolute_error(dvar_test[col],dvar_pred_iv[col])
		MAPE = mean_absolute_percentage_error(dvar_test[col],dvar_pred_iv[col])*100
		MAE_all_dvar.append(MAE)
		MAPE_all_dvar.append(MAPE)

	MAE_all_dvar = np.array(MAE_all_dvar)
	MAPE_all_dvar = np.array(MAPE_all_dvar)
	MAE_all_dvar = pd.DataFrame([MAE_all_dvar],index = ['MAE_dvar'],columns = dvar_columns)
	MAPE_all_dvar = pd.DataFrame([MAPE_all_dvar], index = ['MAPE_dvar'], columns = dvar_columns)
	MAE_all_qoi = []
	MAPE_all_qoi = []
	for col in qoi_test.keys():
		MAE = mean_absolute_error(qoi_test[col],qoi_pred_fw[col])
		MAPE = mean_absolute_percentage_error(qoi_test[col],qoi_pred_fw[col])
		MAE_all_qoi.append(MAE)
		MAPE_all_qoi.append(MAPE)
	MAE_all_qoi = np.array(MAE_all_qoi)
	MAPE_all_qoi = np.array(MAPE_all_qoi)
	MAE_all_qoi = pd.DataFrame([MAE_all_qoi],index = ['MAE_qoi'],columns = qoi_columns)
	MAPE_all_qoi = pd.DataFrame([MAPE_all_qoi],index = ['MAPE_qoi'],columns = qoi_columns)
	# Root mean squared error:
	RMSE_dvar = []
	for col in dvar_test.keys():
		RMSE = np.sqrt(mean_squared_error(dvar_test[col],dvar_pred_iv[col]))
		RMSE_dvar.append(RMSE)
	RMSE_dvar = pd.DataFrame(RMSE_dvar,index = dvar_test.keys()).T
	RMSE_qoi = []
	for col in qoi_test.keys():
		RMSE = np.sqrt(abs(mean_squared_error(qoi_test[col],qoi_pred_fw[col])))
		RMSE_qoi.append(RMSE)
	RMSE_qoi = np.array(RMSE_qoi)
	RMSE_qoi = pd.DataFrame([RMSE_qoi], columns = qoi_columns)
# Explained variance score
	EVS_dvar = []
	for col in dvar_test.keys():
		EVS = explained_variance_score(dvar_test[col],dvar_pred_iv[col])
		EVS_dvar.append(EVS)
	EVS_dvar = pd.DataFrame(EVS_dvar,index = dvar_test.keys()).T
	EVS_all = []
	for col in qoi_test.keys():
		EVS = explained_variance_score(qoi_test[col],qoi_pred_fw[col])
		EVS_all.append(EVS)
	EVS_all_df = pd.DataFrame(EVS_all).T
	EVS_all_df.columns = qoi_test.columns
	
	wl = ['450', '532','630']
	wl_P11_450 = []
	wl_P11_532 = []
	wl_P11_630 = []
	wl_P12_450 = []
	wl_P12_532 = []
	wl_P12_630 = []
	for key in qoi_columns:
		if 'P11_0.450' in key:
			wl_P11_450.append(key)
		elif 'P11_0.532' in key:
			wl_P11_532.append(key)
		elif 'P11_0.630' in key:
			 wl_P11_630.append(key)
		elif 'P12_0.450' in key:
			wl_P12_450.append(key)
		elif 'P12_0.532' in key:
			wl_P12_532.append(key)
		elif 'P12_0.630' in key:
			wl_P12_630.append(key)
 
	wl_list_P11 = [wl_P11_450,wl_P11_532,wl_P11_630]
	wl_list_P12 = [wl_P12_450, wl_P12_532, wl_P12_630]
	
	r2_adj_fw = r2_adj_fw.rename({0: 'r2_adj'}, axis='index')
	r2_adj_iv = r2_adj_iv.rename({0: 'r2_adj'}, axis='index')
	configs = pd.read_csv(f'{model_dir}/configs.csv')
	store_results = pd.DataFrame()
	store_results['data_file'] = configs['config/datafile']
	store_results['batch_size'] = configs['config/batch_size']
	store_results['learning_rate'] = configs['config/learning_rate']
	store_results['n_blocks'] = configs['config/n_blocks']
	store_results['n_depth'] = configs['config/n_depth']
	store_results['n_width'] = configs['config/n_width']
	store_results['weight_x'] = configs['config/weight_x']
	store_results['weight_y'] = configs['config/weight_y']
	store_results['weight_z'] = configs['config/weight_z']
	store_results['weight_artificial'] = configs['config/weight_artificial']
	store_results['weight_reconstruction'] = configs['config/weight_reconstruction']
	store_results['r2_val_qoi_mean'] = configs['r2_val']
	store_results['r2_test_qoi_mean'] = r2_adj_fw.mean(axis =1).values
	if 'P11' in qoi_columns[0]:
		store_results['rel_error95%_qoi_P11_max'] = rel_error_table_fw_P11.max(axis = 1)['95%']
		store_results['abs_error95%_qoi_P11_max'] = abs_error_table_fw_P11.max(axis = 1)['95%']
	else:
		store_results['rel_error95%_qoi_P11_max'] = 0
		store_results['abs_error95%_qoi_P11_max'] = 0
	if 'P12' in qoi_columns[-1]:
		store_results['rel_error95%_qoi_P12_max'] = rel_error_table_fw_P12.max(axis = 1)['95%']
		store_results['abs_error95%_qoi_P12_max'] = abs_error_table_fw_P12.max(axis = 1)['95%']
	else:
		store_results['rel_error95%_qoi_P12_max'] = 0
		store_results['abs_error95%_qoi_P12_max'] = 0
	store_results['MAE_qoi_max'] = MAE_all_qoi.max(axis= 1).values
	store_results['MAPE%_qoi_max'] = MAPE_all_qoi.max(axis= 1).values
	store_results['explained_variance_qoi_min'] = EVS_all_df.min(axis = 1)

	store_results['r2_val_dvar_mean'] = configs['r2_val_dvar']
	store_results['r2_test_dvar_mean'] = r2_adj_iv.mean(axis = 1).values

	store_results['r2_test_dvar_V'] = r2_adj_iv['V_tot']['r2_adj']
	store_results['rel_error95%_dvar_V'] = rel_error_table_iv['V_tot']['95%']
	store_results['abs_error95%_dvar_V'] = abs_error_table_iv['V_tot']['95%']
	store_results['MAE_dvar_V'] = MAE_all_dvar['V_tot'].values
	store_results['MAPE%_dvar_V'] = MAPE_all_dvar['V_tot'].values
	store_results['explained_variance_dvar_V'] = EVS_dvar['V_tot']
	print(rel_error_table_iv)
	store_results['r2_test_dvar_R'] = r2_adj_iv['R_median']['r2_adj']
	store_results['rel_error95%_dvar_R'] = rel_error_table_iv['R_median']['95%']
	store_results['abs_error95%_dvar_R'] = abs_error_table_iv['R_median']['95%']
	store_results['MAE_dvar_R'] = MAE_all_dvar['R_median'].values
	store_results['MAPE%_dvar_R'] = MAPE_all_dvar['R_median'].values
	store_results['explained_variance_dvar_R'] = EVS_dvar['R_median']

	store_results['r2_test_dvar_GSD'] = r2_adj_iv['GSD']['r2_adj']
	store_results['rel_error95%_dvar_GSD'] = rel_error_table_iv['GSD']['95%']
	store_results['abs_error95%_dvar_GSD'] = abs_error_table_iv['GSD']['95%']
	store_results['MAE_dvar_GSD'] = MAE_all_dvar['GSD'].values
	store_results['MAPE%_dvar_GSD'] = MAPE_all_dvar['GSD'].values
	store_results['explained_variance_dvar_GSD'] = EVS_dvar['GSD']

	if 'n' in dvar_columns:
		store_results['r2_test_dvar_n'] = r2_adj_iv['n']['r2_adj']
		store_results['rel_error95%_dvar_n'] = rel_error_table_iv['n']['95%']
		store_results['abs_error95%_dvar_n'] = abs_error_table_iv['n']['95%']
		store_results['MAE_dvar_n'] = MAE_all_dvar['n'].values
		store_results['MAPE%_dvar_n'] = MAPE_all_dvar['n'].values
		store_results['explained_variance_dvar_n'] = EVS_dvar['n']
	else:
		store_results['r2_test_dvar_n'] = 0
		store_results['rel_error95%_dvar_n'] = 0
		store_results['abs_error95%_dvar_n'] = 0
		store_results['MAE_dvar_n'] = 0
		store_results['MAPE%_dvar_n'] = 0
		store_results['explained_variance_dvar_n'] = 0   



	if 'n_532nm' in dvar_columns:
		store_results['r2_test_dvar_n_532'] = r2_adj_iv['n_532nm']['r2_adj']
		store_results['rel_error95%_dvar_n_532'] = rel_error_table_iv['n_532nm']['95%']
		store_results['abs_error95%_dvar_n_532'] = abs_error_table_iv['n_532nm']['95%']
		store_results['MAE_dvar_n_532'] = MAE_all_dvar['n_532nm'].values
		store_results['MAPE%_dvar_n_532'] = MAPE_all_dvar['n_532nm'].values
		store_results['explained_variance_dvar_n_532'] = EVS_dvar['n_532nm']
	else:
		store_results['r2_test_dvar_n_532'] = 0
		store_results['rel_error95%_dvar_n_532'] = 0
		store_results['abs_error95%_dvar_n_532'] = 0
		store_results['MAE_dvar_n_532'] = 0
		store_results['MAPE%_dvar_n_532'] = 0
		store_results['explained_variance_dvar_n_532'] = 0


	if 'k_532nm' in dvar_columns:
		store_results['r2_test_dvar_k_532'] = r2_adj_iv['k_532nm']['r2_adj']
		store_results['rel_error95%_dvar_k_532'] = rel_error_table_iv['k_532nm']['95%']
		store_results['abs_error95%_dvar_k_532'] = abs_error_table_iv['k_532nm']['95%']
		store_results['MAE_dvar_k_532'] = MAE_all_dvar['k_532nm'].values
		store_results['MAPE%_dvar_k_532'] = MAPE_all_dvar['k_532nm'].values
		store_results['explained_variance_dvar_k_532'] = EVS_dvar['k_532nm']
	else:
		store_results['r2_test_dvar_k_532'] = 0
		store_results['rel_error95%_dvar_k_532'] = 0
		store_results['abs_error95%_dvar_k_532'] = 0
		store_results['MAE_dvar_k_532'] = 0
		store_results['MAPE%_dvar_k_532'] = 0
		store_results['explained_variance_dvar_k_532'] = 0
    
	if 'n_450nm' in dvar_columns:
		store_results['r2_test_dvar_n_450'] = r2_adj_iv['n_450nm']['r2_adj']
		store_results['rel_error95%_dvar_n_450'] = rel_error_table_iv['n_450nm']['95%']
		store_results['abs_error95%_dvar_n_450'] = abs_error_table_iv['n_450nm']['95%']
		store_results['MAE_dvar_n_450'] = MAE_all_dvar['n_450nm'].values
		store_results['MAPE%_dvar_n_450'] = MAPE_all_dvar['n_450nm'].values
		store_results['explained_variance_dvar_n_450'] = EVS_dvar['n_450nm'] 
	else:
		store_results['r2_test_dvar_n_450'] = 0
		store_results['rel_error95%_dvar_n_450'] = 0
		store_results['abs_error95%_dvar_n_450'] = 0
		store_results['MAE_dvar_n_450'] = 0
		store_results['MAPE%_dvar_n_450'] = 0
		store_results['explained_variance_dvar_n_450'] = 0

	if 'k_450nm' in dvar_columns:
		store_results['r2_test_dvar_k_450'] = r2_adj_iv['k_450nm']['r2_adj']
		store_results['rel_error95%_dvar_k_450'] = rel_error_table_iv['k_450nm']['95%']
		store_results['abs_error95%_dvar_k_450'] = abs_error_table_iv['k_450nm']['95%']
		store_results['MAE_dvar_k_450'] = MAE_all_dvar['k_450nm'].values
		store_results['MAPE%_dvar_k_450'] = MAPE_all_dvar['k_450nm'].values
		store_results['explained_variance_dvar_k_450'] = EVS_dvar['k_450nm']
	else:
		store_results['r2_test_dvar_k_450'] = 0
		store_results['rel_error95%_dvar_k_450'] = 0
		store_results['abs_error95%_dvar_k_450'] = 0
		store_results['MAE_dvar_k_450'] = 0
		store_results['MAPE%_dvar_k_450'] = 0
		store_results['explained_variance_dvar_k_450'] = 0
    
	if 'n_630nm' in dvar_columns:
		store_results['r2_test_dvar_n_630'] = r2_adj_iv['n_630nm']['r2_adj']
		store_results['rel_error95%_dvar_n_630'] = rel_error_table_iv['n_630nm']['95%']
		store_results['abs_error95%_dvar_n_630'] = abs_error_table_iv['n_630nm']['95%']
		store_results['MAE_dvar_n_630'] = MAE_all_dvar['n_630nm'].values
		store_results['MAPE%_dvar_n_630'] = MAPE_all_dvar['n_630nm'].values
		store_results['explained_variance_dvar_n_630'] = EVS_dvar['n_630nm']
	else:
		store_results['r2_test_dvar_n_630'] = 0
		store_results['rel_error95%_dvar_n_630'] = 0
		store_results['abs_error95%_dvar_n_630'] = 0
		store_results['MAE_dvar_n_630'] =0
		store_results['MAPE%_dvar_n_630'] =0
		store_results['explained_variance_dvar_n_630'] = 0
      
    
	if 'k_630nm' in dvar_columns:
		store_results['r2_test_dvar_k_630'] = r2_adj_iv['k_630nm']['r2_adj']
		store_results['rel_error95%_dvar_k_630'] = rel_error_table_iv['k_630nm']['95%']
		store_results['abs_error95%_dvar_k_630'] = abs_error_table_iv['k_630nm']['95%']
		store_results['MAE_dvar_k_630'] = MAE_all_dvar['k_630nm'].values
		store_results['MAPE%_dvar_k_630'] = MAPE_all_dvar['k_630nm'].values
		store_results['explained_variance_dvar_k_630'] = EVS_dvar['k_630nm']
	else:
		store_results['r2_test_dvar_k_630'] = 0
		store_results['rel_error95%_dvar_k_630'] = 0
		store_results['abs_error95%_dvar_k_630'] = 0
		store_results['MAE_dvar_k_630'] =0
		store_results['MAPE%_dvar_k_630'] =0
		store_results['explained_variance_dvar_k_630'] = 0
        
	if 'k_fine_532' in dvar_columns:
		store_results['r2_test_dvar_k_fine_532'] = r2_adj_iv['k_fine_532']['r2_adj']
		store_results['rel_error95%_dvar_k_fine_532'] = rel_error_table_iv['k_fine_532']['95%']
		store_results['abs_error95%_dvar_k_fine_532'] = abs_error_table_iv['k_fine_532']['95%']
		store_results['MAE_dvar_k_fine_532'] = MAE_all_dvar['k_fine_532'].values
		store_results['MAPE%_dvar_k_fine_532'] = MAPE_all_dvar['k_fine_532'].values
		store_results['explained_variance_dvar_k_fine_532'] = EVS_dvar['k_fine_532']
	else:
		store_results['r2_test_dvar_k_fine_532'] = 0
		store_results['rel_error95%_dvar_k_fine_532'] = 0
		store_results['abs_error95%_dvar_k_fine_532'] = 0
		store_results['MAE_dvar_k_fine_532'] =0
		store_results['MAPE%_dvar_k_fine_532'] =0
		store_results['explained_variance_dvar_k_fine_532'] = 0        
        
	if 'k_coarse_532' in dvar_columns:
		store_results['r2_test_dvar_k_coarse_532'] = r2_adj_iv['k_coarse_532']['r2_adj']
		store_results['rel_error95%_dvar_k_coarse_532'] = rel_error_table_iv['k_coarse_532']['95%']
		store_results['abs_error95%_dvar_k_coarse_532'] = abs_error_table_iv['k_coarse_532']['95%']
		store_results['MAE_dvar_k_coarse_532'] = MAE_all_dvar['k_coarse_532'].values
		store_results['MAPE%_dvar_k_coarse_532'] = MAPE_all_dvar['k_coarse_532'].values
		store_results['explained_variance_dvar_k_coarse_532'] = EVS_dvar['k_coarse_532']
	else:
		store_results['r2_test_dvar_k_coarse_532'] = 0
		store_results['rel_error95%_dvar_k_coarse_532'] = 0
		store_results['abs_error95%_dvar_k_coarse_532'] = 0
		store_results['MAE_dvar_k_coarse_532'] =0
		store_results['MAPE%_dvar_k_coarse_532'] =0
		store_results['explained_variance_dvar_k_coarse_532'] = 0        

	if 'AAE_fine' in dvar_columns:
		store_results['r2_test_dvar_AAE_fine'] = r2_adj_iv['AAE_fine']['r2_adj']
		store_results['rel_error95%_dvar_AAE_fine'] = rel_error_table_iv['AAE_fine']['95%']
		store_results['abs_error95%_dvar_AAE_fine'] = abs_error_table_iv['AAE_fine']['95%']
		store_results['MAE_dvar_AAE_fine'] = MAE_all_dvar['AAE_fine'].values
		store_results['MAPE%_dvar_AAE_fine'] = MAPE_all_dvar['AAE_fine'].values
		store_results['explained_variance_dvar_AAE_fine'] = EVS_dvar['AAE_fine']
	else:
		store_results['r2_test_dvar_AAE_fine'] = 0
		store_results['rel_error95%_dvar_AAE_fine'] = 0
		store_results['abs_error95%_dvar_AAE_fine'] = 0
		store_results['MAE_dvar_AAE_fine'] =0
		store_results['MAPE%_dvar_AAE_fine'] =0
		store_results['explained_variance_dvar_AAE_fine'] = 0        
        
	if 'AAE_coarse' in dvar_columns:
		store_results['r2_test_dvar_AAE_coarse'] = r2_adj_iv['AAE_coarse']['r2_adj']
		store_results['rel_error95%_dvar_AAE_coarse'] = rel_error_table_iv['AAE_coarse']['95%']
		store_results['abs_error95%_dvar_AAE_coarse'] = abs_error_table_iv['AAE_coarse']['95%']
		store_results['MAE_dvar_AAE_coarse'] = MAE_all_dvar['AAE_coarse'].values
		store_results['MAPE%_dvar_AAE_coarse'] = MAPE_all_dvar['AAE_coarse'].values
		store_results['explained_variance_dvar_AAE_coarse'] = EVS_dvar['AAE_coarse']
	else:
		store_results['r2_test_dvar_AAE_coarse'] = 0
		store_results['rel_error95%_dvar_AAE_coarse'] = 0
		store_results['abs_error95%_dvar_AAE_coarse'] = 0
		store_results['MAE_dvar_AAE_coarse'] =0
		store_results['MAPE%_dvar_AAE_coarse'] =0
		store_results['explained_variance_dvar_AAE_coarse'] = 0         
    
	store_results['preprocessor_x'] = configs['config/preprocessor_x']
	store_results['preprocessor_y'] = configs['config/preprocessor_y']
	
	store_results.to_csv(f'{model_dir}/results_summary2.csv')
'''
# -







