# Aerosol_NN_paper

This is the code for the paper "Retrieval of aerosol properties from in situ, multi-angle light scattering measurements using invertible neural networks"

[[_TOC_]]
# Retrieval of Aerosol Properties from Measurement Data


This repository contains the code for building neural network models for the
retrieval of aerosol properties from scattering data.

Forward Problem: Given the aerosol properties compute the phase functions
Inverse Problem: Given the phase functions determine the aerosol properties

Implemented:

- for Forward Problem: surrogate model using dense neural network, Invertible Neural
Network Model

- for Inverse Problem: Invertible Neural Network Model,


## Project Directory Structure

These are the most important directories and sub-directories in the project.

```txt
aerosol
    aerosol_data/
    aerosol_code/ <-- this repo!
    aerosol_results/
    logs
```
The code assumes that this hierarchy is located at `/data/user/boiger_r/aerosol`.
The code is written in a way that hopefully makes it easy to change paths, either
at  the beginning of postprocessing Jupyter notebooks, or as command line arguments
to Python scripts.


## Installation

For PSI users:
- before creating your first environment on Merlin: create in your "/psi/home/$USER/" directory a file, called ".condarc", see https://lsm-hpce.gitpages.psi.ch/merlin6/python.html#condarc-file

- add all conda packages already at the conda create command
- when using pip never use "pip install --user"
- in your ~/.bashrc comment out LD_LIBRARY_PATH...


### Dependencies


``` 
module use unstable
module load anaconda
```

For external users, start here with creating your environment:

It is assumed that you install the code using Anaconda.
Please notice that you have to adapt the path
where your environment will be located.
```bash
conda_env_dir=path/to/your/new/env
```
**IMPORTANT:** All batch scripts in the repo assume that the environment is located at `/data/user/boiger_r/python_env/aerosol_env`


Then, in the same terminal, run the following commands:
Download the newest Python compatible with tensorflow (status: April 2021)
```bash
conda create --prefix $conda_env_dir python=3.8
```
```bash
conda activate $conda_env_dir
```
```bash
# After typing the following command,
# you will have to confirm the installation.
conda install tensorflow numpy pandas seaborn scikit-learn
```
Install plotly (needed for interactive visualisations):

```bash
conda install -c plotly plotly=4.9.0
pip3 install nbconvert
```

```bash
# Install for Jupyter notebooks.
conda install -c conda-forge ipykernel
```
```bash
# Install pymoo with hardware acceleration.
mkdir tmp
cd tmp
git clone https://github.com/msu-coinlab/pymoo
cd pymoo
git checkout 0.4.1
make compile
pip3 install --user .
cd ../..
rm -rf tmp
```
```bash
# Install MLLIB, the machine learning framework developed at PSI.
# Attention with new pip version from Oct2020! eventually you have to use --use-feature=2020-resolver - read carefully what is written during installation
pip3 install --user mllib@git+https://gitlab.psi.ch/adelmann/mllib.git@90e5fc388a12326d1b004f0add833588250e248c#egg=mllib
```
For development of the models, the `ray` library provides a framework
for parallel hyperparameter scans. To install it, run:

```bash
pip install --user ray[tune]
```

### Jupyter kernel

```bash
pip3 install ipykernel
python -m ipykernel install --user --name turning_tables --display-name "Turning Tables"
```
------------------------
**Intel acceleration**
```
pip install intel-tensorflow-avx512==2.4.0 # linux only
```

To install the GCC library `GLIBCXX_3.4.21`, required by tensorflow, run
```
conda install libgcc
```

You can use
```
strings /data/user/boiger_r/aerosol_env_update/lib/libstdc++.so.6.0.26 | grep GLIBCXX
```
to check whether `GLIBCXX_3.4.21` was successfully installed.

Then you need to export it, in order for tensorflow to finds it, using
```
export LD_LIBRARY_PATH=/data/user/boiger_r/aerosol_env_update/lib:$LD_LIBRARY_PATH
```
You should put the above command in your bashrc, e.g. by running
```
echo '# Link to newer GCC library providing GLIBCXX_3.4.21 for tensorflow' >> ~/.bashrc
echo 'export LD_LIBRARY_PATH=/data/user/boiger_r/aerosol_env_update/lib:$LD_LIBRARY_PATH' >> ~/.bashrc
```
otherwise you will have to manually run the above command whenever you open a new session.


The list of all pip and conda packages that should be installed in your environment now, is attached.[package_list.txt](uploads/1ba4b3f99400da658bfccadb02fbf211/package_list.txt)

---------------
Packages I installed later on:
```bash
pip install nbformat
```

```bash
pip install pydot
```
```bash
conda install Jinja2
```

---------------
### Optional - not necessary: Repo
From within the repo root,execute the following command, i.e. install the modules in the project folder:

```bash
pip3 install --user .
```


## Clone the Repository

Go to the gitlab start page of the project, at the right you find a blue icon "Clone", click on it and copy the link at Clone with HTTPS. 
Open a terminal and navigate to the folder, where you want to place your code, then type git clone and paste the link:

```bash
git clone https://gitlab.psi.ch/boiger_r/aerosol_nn_paper.git
```






## Repository description (need to be reworked)
The repository aerosol_NN_paper consists of the folders:
- exploration: Jupyter Notebooks for preprocessing 
- helper_functions: python classes and functions that help you and contain most of the code for building and training NN
- postprocessing: Jupyter Notebooks for analysis
- scripts: python scripts for hyperparameter scan
- slurm_scripts: to run the hyperparameter scan scripts - make your own slurm_scripts!

### Scripts
The folder contains the python files, for performing a hyperparameter scan 
with INN (e.g. hyperparameter_scan_RB_model1.py). The main difference between the files lies in the "nominal_dimension", since all cases have a different number of parameters for the (polarized) phase functions.

Before performing a hyperparameter scan, check the given values for the hyperparameters
and adapt it. 

### Slurm_Scripts
The folder contains all the slurm scripts to run the hyperparameter scans. Please
make your own files to set the right absolute paths. 

For PSI users:

change the following things:
```
#SBATCH --output= $your_output_folder$
#SBATCH --error= $your_error_file_folder$
#SBATCH --job-name= $your_job_name$
conda activate $your_python_environment$
python3 $path_to_your_python_file_for_the_hyperparameter_scan$ 
 --datafile $path_to_your_datafile$ 
 --result_dir $path_to_your_results_folder$ 
 --model $name_of_the_model_you_want_to_run_see_python_file$
```

for the paper only ASHA_invertible was used for the model. The other models are not guaranted here to be completely up to date.



### postprocessing
This folder contains all jupyter notebooks to analyze and explore the data and 
produce the plots for the paper. The numbering of the figures according to the paper is in the naming of the notebooks. 


### helper-functions
- scan_helper_functions.py: 
-- load_dataset: loads and splits the dataset in training, validation and test data
-- metrics are defined: RSquared, AdjustedRSquared, calculate_metrics (metrics are computed) 
-- for the forward model: build_forward_surrogate (function to build the forward model), train_forward_model (function to train the model),..., 
-- for the invertible model: function to train the model

- files used to construct the invertible neural network:
-- affine_coupling_block.py: class that defines the Affine coupling blocks
-- ardizzone_et_al_helpers.py: 
-- invertible_neural_network.py: function to build the invertible neural network,  function to calculate losses and give the inverse pass, a class for training the invertible model and a class for construction the invertible model
-- permutation_layer.py: class that defines the permutation layer


# How to use:
Please change the paths in all the following steps

1.) Preprocessing: With "helper_functions/preprocessing_unimodal_RB_AAE.py", "helper_functions/preprocessing_bimodal_RB_AAE.py" the data can be preprocessed, the scripts are used in the Jupyter Notebook "exploration/preprocessing_unimodal_RB_AAE"

2.) Train a neural network:
for PSI users:
Run a slurm script on Merlin: 
```bash
sbatch slurm_scripts\R_scan_invertible_model_version1.slurm
```

External users: 
run a function "scripts\hyperparameter_scan_RB_model1.py"

3.) Postprocessing:

i) First find best models among all models trained during hyperparameter scan by using the Jupyter Notebook "postprocessing/analyse_hyperparameter_scan_invertible_model_RB_ipynb". The best model (according to R2 values) will be stored in a new folder. 

ii) Use the best model for prediction "postprocessing/best_model_bimodal.py" and store the predictions

iii) Use the stored predictions to compute metrics, make plots:  "postprocessing/invertible_model_RB_prediction_done.ipynb", "postprocessing/invertible_model_RB_prediction_done_AAE_unimodal.ipynb" and all notebooks in "postprocessing" with "Fig" in the name



